# User manual

This is  a template that uses to auto-generate excel manuals based on markdown content.

Please install the most recent java-export-excel package to get best results on your local computer.

## Excel generation

Excel files are generated using Apache POI.

## How to run project

1. Run project with mvn
```
   mvn
```


