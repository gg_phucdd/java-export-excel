/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Aug 28, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Aug 28, 2020
 */
package vn.tcx.app.resources;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * class DemoVM.
 *
 * @author haond
 * @since 1.0
 * @created 20/07/2020 08:28:01
 */
@Data
@AllArgsConstructor
public class DemoVM {
    /**
     * ten.
     */
    private String ten;

    /**
     * loai estimate.
     */
    private String loaiEstimate;

    /**
     * ma.
     */
    private String ma;

    /**
     * trang thai.
     */
    private String trangThai;

    /**
     * total estimate hours.
     */
    private String totalEstimateHours;
}
