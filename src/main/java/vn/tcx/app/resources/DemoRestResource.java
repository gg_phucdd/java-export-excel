/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 16, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 16, 2020
 */

package vn.tcx.app.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import vn.tcx.app.excel.WriteExcelService;
import vn.tcx.app.utils.CommonUtils;
import vn.tcx.app.utils.FileUtils;

/**
 * class DemoRestResource
 * 
 * @author haond
 * @since 1.0
 * @created 16/07/2020 09:46:12
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class DemoRestResource {
    /**
     * excel service
     */
    private final WriteExcelService excelService;

    /**
     * Xuat excel ds du an
     *
     * @author lehuy
     * @since Jul 23, 2020 9:38:53 AM
     * @param chucDanhId
     * @return file excel ds du an
     * @throws IOException 
     */
    @GetMapping(value = "/demo", headers = "X-API-VERSION=1")
    @ApiOperation("Export file excel demo")
    public ResponseEntity<byte[]> xuatExcelDanhSachDuAn() throws IOException {
        List<DemoVM> dsDemoVM =  new ArrayList<>();
        dsDemoVM.add(new DemoVM("demo1", "demo2", "demo3", "demo4", "demo5"));
        byte[] fileOut = excelService.exportExcelDS(new ArrayList<Object>(dsDemoVM), new ClassPathResource("templates/excel/template.xlsx").getFile(), CommonUtils.getDataConfig("templates/excel/config_template.json"));
        return FileUtils.downloadFile("attachment; filename=fileDemo.xlsx", fileOut);
    }
}
