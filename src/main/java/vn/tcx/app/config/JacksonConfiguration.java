/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.problem.ProblemModule;
import org.zalando.problem.violations.ConstraintViolationProblemModule;

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;

/**
 * Class JacksonConfiguration.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
@Configuration
public class JacksonConfiguration {
    /**
     * Support for Java date and time API.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the corresponding Jackson module.
     */
    @Bean
    public JavaTimeModule javaTimeModule() {
        return new JavaTimeModule();
    }

    /**
     * Jdk 8 time module.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the jdk 8 module
     */
    @Bean
    public Jdk8Module jdk8TimeModule() {
        return new Jdk8Module();
    }

    /**
     * Support for Hibernate types in Jackson.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the hibernate 5 module
     */
    @Bean
    public Hibernate5Module hibernate5Module() {
        return new Hibernate5Module();
    }

    /**
     * Jackson Afterburner module to speed up serialization/deserialization.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the afterburner module
     */
    @Bean
    public AfterburnerModule afterburnerModule() {
        return new AfterburnerModule();
    }

    /**
     * Module for serialization/deserialization of RFC7807 Problem.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the problem module
     */
    @Bean
    ProblemModule problemModule() {
        return new ProblemModule();
    }

    /**
     * Module for serialization/deserialization of ConstraintViolationProblem.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @return the constraint violation problem module
     */
    @Bean
    ConstraintViolationProblemModule constraintViolationProblemModule() {
        return new ConstraintViolationProblemModule();
    }
}
