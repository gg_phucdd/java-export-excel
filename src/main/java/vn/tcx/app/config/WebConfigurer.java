package vn.tcx.app.config;

import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jun 1, 2020
 */
@Configuration
public class WebConfigurer implements ServletContextInitializer, WebServerFactoryCustomizer<WebServerFactory> {
    /**
     * log.
     */
    private final Logger log = LoggerFactory.getLogger(WebConfigurer.class);

    /**
     * env.
     */
    private final Environment env;

    /**
     * Instantiates a new web configurer.
     *
     * @update PhucDD
     * @lastModifier Jun 1, 2020
     * @param env the env
     */
    public WebConfigurer(Environment env) {
        this.env = env;
    }

    /**
     * On startup.
     *
     * @update PhucDD
     * @lastModifier Jun 1, 2020
     * @param servletContext the servlet context
     * @throws ServletException servlet exception
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        if (env.getActiveProfiles().length != 0) {
            log.info("Web application configuration, using profiles: {}", (Object[]) env.getActiveProfiles());
        }
        log.info("Web application fully configured");
    }

    /**
     * Customize the Servlet engine: Mime types, the document root, the cache.
     *
     * @update PhucDD
     * @lastModifier Jun 1, 2020
     * @param server the server
     */
    @Override
    public void customize(WebServerFactory server) {
        setMimeMappings(server);
    }

    /**
     * Sets the mime mappings.
     *
     * @param server the new mime mappings
     */
    private void setMimeMappings(WebServerFactory server) {
        if (server instanceof ConfigurableServletWebServerFactory) {
            MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
            // IE issue, see https://github.com/jhipster/generator-jhipster/pull/711
            mappings.add("html", MediaType.TEXT_HTML_VALUE + ";charset=" + StandardCharsets.UTF_8.name().toLowerCase());
            // CloudFoundry issue, see https://github.com/cloudfoundry/gorouter/issues/64
            mappings.add("json", MediaType.TEXT_HTML_VALUE + ";charset=" + StandardCharsets.UTF_8.name().toLowerCase());
            ConfigurableServletWebServerFactory servletWebServer = (ConfigurableServletWebServerFactory) server;
            servletWebServer.setMimeMappings(mappings);
        }
    }

    /**
     * Cors filter.
     *
     * @update PhucDD
     * @lastModifier Jun 1, 2020
     * @return the cors filter
     */
    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = getConfigurationSource();
        return new CorsFilter(source);
    }

    /**
     * Lay configuration source.
     *
     * @update GreenGlobal
     * @lastModifier Jun 1, 2020
     * @return configuration source
     */
    @Bean
    public UrlBasedCorsConfigurationSource getConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addExposedHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS);
        config.addExposedHeader(HttpHeaders.CONTENT_DISPOSITION);
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("HEAD");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("DELETE");
        config.addAllowedMethod("PATCH");
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    
    /**
     * Class WebConfig.
     *
     * @author GreenGlobal
     * @since 1.0
     * @created Jun 1, 2020
     */
    @Configuration
    public static class WebConfig implements WebMvcConfigurer {

        /**
         * Adds the argument resolvers.
         *
         * @update PhucDD
         * @lastModifier Jun 1, 2020
         * @param argumentResolvers the argument resolvers
         */
        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
            PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
            resolver.setMaxPageSize(Integer.MAX_VALUE);
            resolver.setFallbackPageable(PageRequest.of(0, 10));
            argumentResolvers.add(resolver);
        }
    }
}
