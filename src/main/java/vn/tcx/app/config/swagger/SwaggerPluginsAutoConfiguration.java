/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.config.swagger;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;

import springfox.documentation.spring.web.plugins.Docket;

/**
 * Register Springfox plugins.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnBean(Docket.class)
@AutoConfigureAfter(SwaggerAutoConfiguration.class)
public class SwaggerPluginsAutoConfiguration {
    /**
     * Class SpringPagePluginConfiguration.
     *
     * @author GreenGlobal
     * @since 1.0
     * @created Jul 13, 2020
     */
    @Configuration
    @ConditionalOnClass(Pageable.class)
    public static class SpringPagePluginConfiguration {
    }
}
