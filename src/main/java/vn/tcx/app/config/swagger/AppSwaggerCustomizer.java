/*
 * Copyright 2016-2017 the original author or authors from the JHipster project.
 *
 * This file is part of the JHipster project, see https://www.jhipster.tech/
 * for more information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vn.tcx.app.config.swagger;

import static springfox.documentation.builders.PathSelectors.regex;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;

import org.springframework.core.Ordered;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.Docket;
import vn.tcx.app.config.Properties;

/**
 * A swagger customizer to setup {@link Docket} with TCX settings.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
public class AppSwaggerCustomizer implements SwaggerCustomizer, Ordered {
    /**
     * The default order for the customizer.
     */
    public static final int DEFAULT_ORDER = 0;

    /**
     * order.
     */
    private int order = DEFAULT_ORDER;

    /**
     * properties.
     */
    private final Properties.Swagger properties;

    /**
     * Instantiates a new app swagger customizer.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param properties the properties
     */
    public AppSwaggerCustomizer(Properties.Swagger properties) {
        this.properties = properties;
    }

    /**
     * Customize.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param docket the docket
     */
    @Override
    public void customize(Docket docket) {

        ApiInfo apiInfo = new ApiInfoBuilder().title(properties.getTitle())
                .description(properties.getDescription())
                .version(properties.getVersion()).build();

        docket.host(properties.getHost()).protocols(new HashSet<>(Arrays.asList(properties.getProtocols())))
                .apiInfo(apiInfo).useDefaultResponseMessages(properties.isUseDefaultResponseMessages())
                .forCodeGeneration(true).directModelSubstitute(ByteBuffer.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class).ignoredParameterTypes(Pageable.class).select()
                .paths(regex(properties.getDefaultIncludePattern())).build();
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * Lay order.
     *
     * @update GreenGlobal
     * @lastModifier Jul 13, 2020
     * @return order
     */
    @Override
    public int getOrder() {
        return order;
    }
}
