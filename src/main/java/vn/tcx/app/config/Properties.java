/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

import lombok.Getter;
import lombok.Setter;

/**
 * Class PageableParameterBuilderPlugin.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = true)
public class Properties {
    /**
     * file upload dir.
     */
    private String fileUploadDir = "";

    /**
     * send activated email.
     */
    private boolean sendActivatedEmail = false;

    /**
     * cache.
     */
    private final Cache cache = new Cache();

    /**
     * swagger.
     */
    private final Swagger swagger = new Swagger();

    /**
     * security.
     */
    private final Security security = new Security();

    /**
     * cors.
     */
    private final CorsConfiguration cors = new CorsConfiguration();

    /**
     * mail.
     */
    private final Mail mail = new Mail();

    /**
     * base url.
     */
    private final BaseUrl baseUrl = new BaseUrl();

    /**
     * Get ehcache.
     *
     * @update GreenGlobal
     * @lastModifier May 29, 2020
     * @return ehcache
     */
    @Getter
    public static class Cache {
        /**
         * ehcache.
         */
        private final Ehcache ehcache = new Ehcache();

        /**
         * Get key.
         *
         * @update GreenGlobal
         * @lastModifier May 29, 2020
         * @return key
         */
        @Getter
        @Setter
        public static class Ehcache {

            /**
             * time to live seconds.
             */
            private int timeToLiveSeconds = 3600;

            /**
             * max entries.
             */
            private long maxEntries = 100;
        }
    }

    /**
     * Get Swagger.
     *
     * @update GreenGlobal
     * @lastModifier May 29, 2020
     * @return key
     */
    @Getter
    @Setter
    public static class Swagger {
        /**
         * title.
         */
        private String title = "Application API";

        /**
         * description.
         */
        private String description = "API Documentation";

        /**
         * version.
         */
        private String version = "1.0.0";

        /**
         * host.
         */
        private String host;

        /**
         * protocols.
         */
        private String[] protocols = {};

        /**
         * use default response messages.
         */
        private boolean useDefaultResponseMessages = true;

        /**
         * default include pattern.
         */
        private String defaultIncludePattern = "/api/.*";

    }

    /**
     * Get remember me.
     *
     * @update GreenGlobal
     * @lastModifier May 29, 2020
     * @return remember me
     */
    @Getter
    @Setter
    public static class Security {

        /**
         * client authorization.
         */
        private final ClientAuthorization clientAuthorization = new ClientAuthorization();

        /**
         * authentication.
         */
        private final Authentication authentication = new Authentication();

        /**
         * remember me.
         */
        private final RememberMe rememberMe = new RememberMe();

        /**
         * Get ClientAuthorization.
         *
         * @update GreenGlobal
         * @lastModifier May 29, 2020
         * @return key
         */
        @Getter
        @Setter
        public static class ClientAuthorization {

            /**
             * access token uri.
             */
            private String accessTokenUri = null;

            /**
             * token service id.
             */
            private String tokenServiceId = null;

            /**
             * client id.
             */
            private String clientId = null;

            /**
             * client secret.
             */
            private String clientSecret = null;
        }

        /**
         * Class Authentication.
         *
         * @author GreenGlobal
         * @since 1.0
         * @created May 29, 2020
         */
        public static class Authentication {

            /**
             * jwt.
             */
            private final Jwt jwt = new Jwt();

            /**
             * Get jwt.
             *
             * @update GreenGlobal
             * @lastModifier May 29, 2020
             * @return jwt
             */
            public Jwt getJwt() {
                return jwt;
            }

            /**
             * oauth 2.
             */
            private final Oauth2 oauth2 = new Oauth2();

            /**
             * Get oauth 2.
             *
             * @update GreenGlobal
             * @lastModifier May 29, 2020
             * @return oauth 2
             */
            public Oauth2 getOauth2() {
                return oauth2;
            }

            /**
             * Get Jwt.
             *
             * @update GreenGlobal
             * @lastModifier May 29, 2020
             * @return key
             */
            @Getter
            @Setter
            public static class Jwt {
                /**
                 * secret.
                 */
                private String secret = null;

                /**
                 * base 64 secret.
                 */
                private String base64Secret = null;

                /**
                 * token validity in seconds. 0.5 hour
                 */
                private long tokenValidityInSeconds = 1800;

                /**
                 * token validity in seconds for remember me. 30 hours
                 */
                private long tokenValidityInSecondsForRememberMe = 2592000;
            }

            /**
             * Get Oauth2.
             *
             * @update GreenGlobal
             * @lastModifier May 29, 2020
             * @return jwt
             */
            @Getter
            @Setter
            public static class Oauth2 {
                /**
                 * google
                 */
                private final Google google = new Google();

                /**
                 * Get google.
                 *
                 * @update GreenGlobal
                 * @lastModifier May 29, 2020
                 * @return oauth 2
                 */
                public Google getGoogle() {
                    return google;
                } 

                /**
                 * grant type.
                 */
                private String grantType = null;

                /**
                 * principal attribute.
                 */
                private String principalAttribute = null;

                /**
                 * authority attribute.
                 */
                private String authorityAttribute = null;

                /**
                 * client id.
                 */
                private String clientId = null;

                /**
                 * client secret.
                 */
                private String clientSecret = null;

                /**
                 * access token uri.
                 */
                private String accessTokenUri = null;

                /**
                 * check token endpoint.
                 */
                private String checkTokenEndpoint = null;

                /**
                 * user info uri.
                 */
                private String userInfoUri = null;
                
                /**
                 * google
                 *
                 * @author phucdd
                 */
                @Getter
                @Setter
                public static class Google {
                    /**
                     * client id.
                     */
                    private String clientId = null;

                    /**
                     * client secret.
                     */
                    private String clientSecret = null;

                    /**
                     * redirect uri.
                     */
                    private String redirectUri = null;
                    
                    /**
                     * token.
                     */
                    private String token = null;

                    /**
                     * user info.
                     */
                    private String userInfo = null;
                }
            }
        }

        /**
         * Get RememberMe.
         *
         * @update GreenGlobal
         * @lastModifier May 29, 2020
         * @return key
         */
        @Getter
        @Setter
        public static class RememberMe {

            /**
             * key.
             */
            @NotNull
            private String key = null;
        }
    }

    /**
     * Get Mail.
     *
     * @update GreenGlobal
     * @lastModifier May 29, 2020
     * @return base url
     */
    @Getter
    @Setter
    public static class Mail {

        /**
         * enabled.
         */
        private boolean enabled = false;

        /**
         * from.
         */
        private String from = "";

        /**
         * username
         */
        private String username = "";

        /**
         * title
         */
        private String title = "";

        /**
         * base url.
         */
        private String baseUrl = "";

        /**
         * frontEndUrl
         */
        private String frontEndUrl = "";

        /**
         * isMultipart
         */
        private boolean isMultipart = false;

    }

    /**
     * Get api.
     *
     * @update GreenGlobal
     * @lastModifier May 29, 2020
     * @return api
     */
    @Setter
    @Getter
    public static class BaseUrl {

        /**
         * frontend.
         */
        private String frontend = "";

        /**
         * api.
         */
        private String api = "";
    }
}
