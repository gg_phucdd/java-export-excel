/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Class DatabaseConfiguration.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
@Configuration
@EnableTransactionManagement
@EnableSpringDataWebSupport
public class DatabaseConfiguration {

}
