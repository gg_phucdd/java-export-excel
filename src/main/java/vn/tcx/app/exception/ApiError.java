/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.exception;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Lay timestamp.
 *
 * @update GreenGlobal
 * @lastModifier Jul 13, 2020
 * @return timestamp
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class ApiError {
    /**
     * status.
     */
    private HttpStatus status;

    /**
     * message.
     */
    private String message;

    /**
     * code.
     */
    private String code;

    /**
     * app code.
     */
    private String appCode;

    /**
     * app status.
     */
    private String appStatus;

    /**
     * app message.
     */
    private String appMessage;

    /**
     * errors.
     */
    private List<String> errors;

    /**
     * timestamp.
     */
    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp = LocalDateTime.now();

    /**
     * Instantiates a new api error.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     */
    public ApiError() {
        super();
    }

    /**
     * Instantiates a new api error.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param status  the status
     * @param message the message
     * @param errors  the errors
     */
    public ApiError(final HttpStatus status, final String message, final List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    /**
     * Instantiates a new api error.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param status  the status
     * @param message the message
     * @param error   the error
     */
    public ApiError(final HttpStatus status, final String message, final String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }

    /**
     * Instantiates a new api error.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param status  the status
     * @param message the message
     * @param code    the code
     * @param errors  the errors
     */
    public ApiError(final HttpStatus status, final String message, final String code, final List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.code = code;
        this.errors = errors;
    }

    /**
     * Instantiates a new api error.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param status  the status
     * @param message the message
     * @param code    the code
     * @param error   the error
     */
    public ApiError(final HttpStatus status, final String message, final String code, final String error) {
        super();
        this.status = status;
        this.message = message;
        this.code = code;
        errors = Arrays.asList(error);
    }
}
