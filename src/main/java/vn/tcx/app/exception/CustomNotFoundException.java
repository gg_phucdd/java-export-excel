/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 13, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 13, 2020
 */

package vn.tcx.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class CustomNotFoundException.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 13, 2020
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CustomNotFoundException extends RuntimeException {
    /**
     * Constant serialVersionUID.
     */
    private static final long serialVersionUID = 852483892970155284L;

    /**
     * Instantiates a new custom not found exception.
     *
     * @update PhucDD
     * @lastModifier Jul 13, 2020
     * @param message the message
     */
    public CustomNotFoundException(String message) {
        super(message);
    }
}
