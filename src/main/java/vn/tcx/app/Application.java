/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 7, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 7, 2020
 */

package vn.tcx.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class Application.
 */
@SpringBootApplication
public class Application {
    /**
     * Main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
