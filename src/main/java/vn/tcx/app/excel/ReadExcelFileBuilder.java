/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Apr 6, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Apr 6, 2020
 */

package vn.tcx.app.excel;

/**
 * Interface ReadExcelFileBuilder.
 *
 * @author PhucDD
 * @since 1.0
 * @created Apr 6, 2020
 */
public interface ReadExcelFileBuilder {

}
