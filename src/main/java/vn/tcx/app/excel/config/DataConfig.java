/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 22, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 22, 2020
 */

package vn.tcx.app.excel.config;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Builder;
import lombok.Data;

/**
 * Instantiates a new data config.
 *
 * @update PhucDD
 * @lastModifier Jul 22, 2020
 */
@Data
@Builder
@JsonDeserialize(builder = DataConfig.DataConfigBuilder.class)
public class DataConfig {
    /**
     * row start.
     */
    @JsonProperty("rowStart")
    private int rowStart;

    /**
     * rows.
     */
    @JsonProperty("rows")
    private List<RowsConfig> rows;
}