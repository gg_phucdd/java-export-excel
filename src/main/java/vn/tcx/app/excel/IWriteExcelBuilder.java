/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Apr 6, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Apr 6, 2020
 */

package vn.tcx.app.excel;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * Interface IWriteExcelBuilder.
 *
 * @author PhucDD
 * @since 1.0
 * @created Apr 6, 2020
 */
public interface IWriteExcelBuilder {

    /**
     * Start sheet.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param name the name
     */
    public void startSheet(String name);

    /**
     * Start sheet.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param sheet     the sheet
     * @param rowIdx    the row idx
     * @param columnIdx the column idx
     */
    public void startSheet(Sheet sheet, int rowIdx, int columnIdx);

    /**
     * Sets the auto size column.
     *
     * @param idx the new auto size column
     */
    public void setAutoSizeColumn(int idx);

    /**
     * Sets the column size.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param idx the idx
     * @param m   the m
     */
    public void setColumnSize(int idx, int m);

    /**
     * Start row.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void startRow();

    /**
     * Sets the row thin top border.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void setRowThinTopBorder();

    /**
     * Sets the row thick top border.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void setRowThickTopBorder();

    /**
     * Sets the row thin bottom border.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void setRowThinBottomBorder();

    /**
     * Sets the row thick bottom border.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void setRowThickBottomBorder();

    /**
     * Sets the row title height.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public void setRowTitleHeight();

    /**
     * Adds the title text column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addTitleTextColumn(String text);

    /**
     * Adds the text left aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addTextLeftAlignedColumn(String text);

    /**
     * Adds the text right aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addTextRightAlignedColumn(String text);

    /**
     * Adds the text center aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addTextCenterAlignedColumn(String text);

    /**
     * Adds the double center aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param val the val
     */
    public void addDoubleCenterAlignedColumn(double val);

    /**
     * Adds the double right aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param val the val
     */
    public void addDoubleRightAlignedColumn(double val);

    /**
     * Adds the integer center aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param val the val
     */
    public void addIntegerCenterAlignedColumn(int val);

    /**
     * Adds the integer right aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param val the val
     */
    public void addIntegerRightAlignedColumn(int val);

    /**
     * Adds the bold text left aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addBoldTextLeftAlignedColumn(String text);

    /**
     * Adds the bold text center aligned column.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param text the text
     */
    public void addBoldTextCenterAlignedColumn(String text);

    /**
     * Sets the content.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     * @param data        the data
     */
    public void setContent(Object[][] data);

    /**
     * Builds the.
     *
     * @update PhucDD
     * @lastModifier Apr 6, 2020
     */
    public byte[] build();
}
