/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 22, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 22, 2020
 */

package vn.tcx.app.excel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enterprisemath.utils.ValidationUtils;

import vn.tcx.app.exception.CustomNotFoundException;
import vn.tcx.app.utils.CommonConstants;

/**
 * Excel report builder.
 *
 * @author radek.hecl
 */
public class WriteExcelBuilder implements IWriteExcelBuilder {

    /**
     * log.
     */
    private final Logger log = LoggerFactory.getLogger(WriteExcelBuilder.class);

    /**
     * Underlined workbook.
     */
    private Workbook workbook;
    /**
     * Current sheet.
     */
    private Sheet sheet = null;
    /**
     * Current row.
     */
    private Row row = null;
    /**
     * Next row index.
     */
    private int nextRowIdx = 0;
    /**
     * Style attributes for row.
     */
    private Set<StyleAttribute> rowStyleAttributes;
    /**
     * Next column index.
     */
    private int nextColumnIdx = 0;

    /**
     * style bank.
     */
    private Map<Set<StyleAttribute>, CellStyle> styleBank = new HashMap<>();

    /**
     * Creates new instance.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public WriteExcelBuilder() {
    }

    /**
     * Starts sheet.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param name sheet name
     */
    public void startSheet(String name) {
        sheet = workbook.createSheet(name);
        nextRowIdx = 0;
        nextColumnIdx = 0;
        rowStyleAttributes = new HashSet<>();
    }

    /**
     * Start sheet.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param sheet     the sheet
     * @param rowIdx    the row idx
     * @param columnIdx the column idx
     */
    public void startSheet(Sheet sheet, int rowIdx, int columnIdx) {
        this.sheet = sheet;
        nextRowIdx = rowIdx;
        nextColumnIdx = columnIdx;
        rowStyleAttributes = new HashSet<>();
    }

    /**
     * Sets auto sizing columns.
     *
     * @param idx column index, starting from 0
     */
    public void setAutoSizeColumn(int idx) {
        sheet.autoSizeColumn(idx);
    }

    /**
     * Sets column size.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param idx column index, starting from 0
     * @param m   number of 'M' standard characters to use for size calculation
     */
    public void setColumnSize(int idx, int m) {
        sheet.setColumnWidth(idx, (m + 1) * 256);
    }

    /**
     * Starts new row.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void startRow() {
        row = sheet.createRow(nextRowIdx);
        nextRowIdx = nextRowIdx + 1;
        nextColumnIdx = 0;
        rowStyleAttributes = new HashSet<>();
    }

    /**
     * Sets row top border as thin.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void setRowThinTopBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, "must be called before inserting columns");
        row.setRowStyle(getCellStyle(StyleAttribute.THIN_TOP_BORDER));
        rowStyleAttributes.add(StyleAttribute.THIN_TOP_BORDER);
    }

    /**
     * Sets row top border as thick.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void setRowThickTopBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, "must be called before inserting columns");
        row.setRowStyle(getCellStyle(StyleAttribute.THICK_TOP_BORDER));
        rowStyleAttributes.add(StyleAttribute.THICK_TOP_BORDER);
    }

    /**
     * Sets row bottom border as thin.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void setRowThinBottomBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, "must be called before inserting columns");
        row.setRowStyle(getCellStyle(StyleAttribute.THIN_BOTTOM_BORDER));
        rowStyleAttributes.add(StyleAttribute.THIN_BOTTOM_BORDER);
    }

    /**
     * Sets row bottom border as thick.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void setRowThickBottomBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, "must be called before inserting columns");
        row.setRowStyle(getCellStyle(StyleAttribute.THICK_BOTTOM_BORDER));
        rowStyleAttributes.add(StyleAttribute.THICK_BOTTOM_BORDER);
    }

    /**
     * Sets row height to capture the title.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public void setRowTitleHeight() {
        ValidationUtils.guardEquals(0, nextColumnIdx, "must be called before inserting columns");
        row.setHeightInPoints(30);
    }

    /**
     * Adds title column.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addTitleTextColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.TITLE_SIZE, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple left aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addTextLeftAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_LEFT);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addTextRightAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple center aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addTextCenterAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple center aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param val value
     */
    public void addDoubleCenterAlignedColumn(double val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param val value
     */
    public void addDoubleRightAlignedColumn(double val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat("#,##0"));
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param dateStr the date str
     */
    public void addZonedDateTimeRightAlignedColumn(String dateStr) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        cell.setCellStyle(style);
        cell.setCellValue(dateStr);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple center aligned int.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param val value
     */
    public void addIntegerCenterAlignedColumn(int val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned int. String rptName,
     * 
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param val value
     */
    public void addIntegerRightAlignedColumn(int val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat("#,##0"));
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds bold left aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addBoldTextLeftAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_LEFT, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds bold center aligned text.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param text text
     */
    public void addBoldTextCenterAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Tao style normal text cho cell va text canh giua
     *
     * @author lehuy
     * @since Jul 23, 2020 2:44:59 PM
     * @param object
     */
    public void addNormalTextCenterAlignedColumn(Object object) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER, StyleAttribute.BORDER_ALL);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(object != null ? object.toString() : CommonConstants.EMPTY_STRING));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Sets the content.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param data the data
     */
    public void setContent(Object[][] data) {
        for (Object[] book : data) {
            startRow();
            for (Object field : book) {
                if (field instanceof String) {
                    addTextLeftAlignedColumn((String) field);
                } else if (field instanceof Integer) {
                    addIntegerRightAlignedColumn((Integer) field);
                } else if (field instanceof Double) {
                    addDoubleRightAlignedColumn((Double) field);
                } else if (field instanceof ZonedDateTime) {
                    addZonedDateTimeRightAlignedColumn(String.valueOf(
                            DateTimeFormatter.ofPattern(CommonConstants.DD_MM_YYYY).format((ZonedDateTime) field)));
                } else {
                    addTextRightAlignedColumn((String) field);
                }
            }
        }
    }

    /**
     * Set style cho cell excel ds du an
     *
     * @author lehuy
     * @since Jul 23, 2020 1:48:40 PM
     * @param data
     */
    public void setContentForDSDuAn(Object[][] data) {
        for (Object[] cellData : data) {
            startRow();
            for (Object field : cellData) {
                if (field instanceof String) {
                    addNormalTextCenterAlignedColumn(field);
                } else if (field instanceof Integer) {
                    addNormalTextCenterAlignedColumn(field);
                } else if (field instanceof Double) {
                    addNormalTextCenterAlignedColumn(field);
                } else if (field instanceof ZonedDateTime) {
                    addNormalTextCenterAlignedColumn(String.valueOf(
                            DateTimeFormatter.ofPattern(CommonConstants.DD_MM_YYYY).format((ZonedDateTime) field)));
                } else {
                    addNormalTextCenterAlignedColumn(field);
                }
            }
        }
    }

    /**
     * Builds the result object. Object cannot be reused after calling this method.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     */
    public byte[] build() {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            workbook.write(bos);
            return bos.toByteArray();
        } catch (IOException e) {
            throw new CustomNotFoundException(e.toString());
        }
    }

    /**
     * Returns cell style.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @param attrs attributes
     * @return cell style
     */
    private CellStyle getCellStyle(StyleAttribute... attrs) {
        Set<StyleAttribute> allattrs = new HashSet<>();
        allattrs.addAll(rowStyleAttributes);
        allattrs.addAll(Arrays.asList(attrs));
        if (styleBank.containsKey(allattrs)) {
            return styleBank.get(allattrs);
        }
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        for (StyleAttribute attr : allattrs) {
            if (attr.equals(StyleAttribute.TITLE_SIZE)) {
                font.setFontHeightInPoints((short) 18);
            } else if (attr.equals(StyleAttribute.BOLD)) {
                font.setBold(true);
            } else if (attr.equals(StyleAttribute.THIN_TOP_BORDER)) {
                style.setBorderTop(BorderStyle.THIN);
            } else if (attr.equals(StyleAttribute.THIN_BOTTOM_BORDER)) {
                style.setBorderBottom(BorderStyle.THIN);
            } else if (attr.equals(StyleAttribute.THICK_TOP_BORDER)) {
                style.setBorderTop(BorderStyle.THICK);
            } else if (attr.equals(StyleAttribute.THICK_BOTTOM_BORDER)) {
                style.setBorderBottom(BorderStyle.THICK);
            } else if (attr.equals(StyleAttribute.ALIGN_LEFT)) {
                style.setAlignment(HorizontalAlignment.LEFT);
            } else if (attr.equals(StyleAttribute.ALIGN_CENTER)) {
                style.setAlignment(HorizontalAlignment.CENTER);
            } else if (attr.equals(StyleAttribute.ALIGN_RIGHT)) {
                style.setAlignment(HorizontalAlignment.RIGHT);
            } else if (attr.equals(StyleAttribute.BORDER_ALL)) {
                style.setBorderTop(BorderStyle.THIN);
                style.setBorderBottom(BorderStyle.THIN);
                style.setBorderLeft(BorderStyle.THIN);
                style.setBorderRight(BorderStyle.THIN);
            }
        }
        styleBank.put(allattrs, style);
        return style;
    }

    /**
     * To string.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @return the string
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Possible style attributes.
     *
     * @author GreenGlobal
     * @since 1.0
     * @created Jul 22, 2020
     */
    private enum StyleAttribute {
        /**
         * Thin top border.
         */
        THIN_TOP_BORDER,
        /**
         * Thin bottom border.
         */
        THIN_BOTTOM_BORDER,
        /**
         * Thick top border.
         */
        THICK_TOP_BORDER,
        /**
         * Thick bottom border.
         */
        THICK_BOTTOM_BORDER,
        /**
         * Title font size.
         */
        TITLE_SIZE,
        /**
         * Bold font.
         */
        BOLD,
        /**
         * Left alignment.
         */
        ALIGN_LEFT,
        /**
         * Center alignment.
         */
        ALIGN_CENTER,
        /**
         * Right alignment.
         */
        ALIGN_RIGHT,
        /**
         * BORDER_ALL
         */
        BORDER_ALL
    }

    /**
     * Lay workbook.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return workbook
     */
    public Workbook getWorkbook() {
        return workbook;
    }

    /**
     * Sets the workbook.
     *
     * @param workbook the new workbook
     */
    public void setWorkbook(Workbook workbook) {
        this.workbook = workbook;
    }

    /**
     * Sets the workbook.
     *
     * @param excelFile the new workbook
     */
    public void setWorkbook(File excelFile) {
        try (FileInputStream excelInputStream = new FileInputStream(excelFile)) {
            if (excelFile.getName().endsWith(CommonConstants.XLSX)) {
                workbook = new XSSFWorkbook(excelInputStream);
            } else if (excelFile.getName().endsWith(CommonConstants.XLS)) {
                workbook = new HSSFWorkbook(excelInputStream);
            }
        } catch (IOException e) {
            log.debug("setWorkbook IOException {}", e.getMessage());
        }
    }

    /**
     * Lay sheet.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return sheet
     */
    public Sheet getSheet() {
        return sheet;
    }

    /**
     * Sets the sheet.
     *
     * @param sheet the new sheet
     */
    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    /**
     * Lay row.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return row
     */
    public Row getRow() {
        return row;
    }

    /**
     * Sets the row.
     *
     * @param row the new row
     */
    public void setRow(Row row) {
        this.row = row;
    }

    /**
     * Lay next row idx.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return next row idx
     */
    public int getNextRowIdx() {
        return nextRowIdx;
    }

    /**
     * Sets the next row idx.
     *
     * @param nextRowIdx the new next row idx
     */
    public void setNextRowIdx(int nextRowIdx) {
        this.nextRowIdx = nextRowIdx;
    }

    /**
     * Lay row style attributes.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return row style attributes
     */
    public Set<StyleAttribute> getRowStyleAttributes() {
        return rowStyleAttributes;
    }

    /**
     * Sets the row style attributes.
     *
     * @param rowStyleAttributes the new row style attributes
     */
    public void setRowStyleAttributes(Set<StyleAttribute> rowStyleAttributes) {
        this.rowStyleAttributes = rowStyleAttributes;
    }

    /**
     * Lay next column idx.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return next column idx
     */
    public int getNextColumnIdx() {
        return nextColumnIdx;
    }

    /**
     * Sets the next column idx.
     *
     * @param nextColumnIdx the new next column idx
     */
    public void setNextColumnIdx(int nextColumnIdx) {
        this.nextColumnIdx = nextColumnIdx;
    }

    /**
     * Lay style bank.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @return style bank
     */
    public Map<Set<StyleAttribute>, CellStyle> getStyleBank() {
        return styleBank;
    }

    /**
     * Sets the style bank.
     *
     * @update PhucDD
     * @lastModifier Jul 22, 2020
     * @param styleBank the style bank
     */
    public void setStyleBank(Map<Set<StyleAttribute>, CellStyle> styleBank) {
        this.styleBank = styleBank;
    }
}