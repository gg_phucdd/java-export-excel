/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Apr 6, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Apr 6, 2020
 */

package vn.tcx.app.excel;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import vn.tcx.app.excel.config.DataConfig;
import vn.tcx.app.utils.CommonConstants;

/**
 * Class WriteExcelService.
 *
 * @author PhucDD
 * @since 1.0
 * @created Apr 6, 2020
 */
@Slf4j
@Service
public class WriteExcelService {
    /**
     * Tao file excel ds du an
     *
     * @author lehuy
     * @since Jul 23, 2020 9:53:38 AM
     * @param objects
     * @param fileNameExcel
     * @param dataConfig
     * @return byte[] danh sach du an
     */
    public byte[] exportExcelDS(List<Object> objects, File fileNameExcel, DataConfig dataConfig) {
        // Create header for excel
        Object[][] data = new Object[objects.size()][dataConfig.getRows().size()];
        for (int index = 0; index < objects.size(); index++) {
            for (int j = 0; j < dataConfig.getRows().size(); j++) {
                try {
                    if (dataConfig.getRows().get(j).isAutoIncrement()) {
                        data[index][dataConfig.getRows().get(j).getCol()] = index + CommonConstants.NUM_1;
                    } else {
                        Field field = objects.get(index).getClass()
                                .getDeclaredField(dataConfig.getRows().get(j).getField());
                        field.setAccessible(true);
                        data[index][dataConfig.getRows().get(j).getCol()] = field.get(objects.get(index));
                    }
                } catch (Exception e) {
                    log.error("Exception function export excel danh sach {0}", e.getMessage());
                }
            }
        }

        WriteExcelBuilder writeExcelBuilder = new WriteExcelBuilder();
        writeExcelBuilder.setWorkbook(fileNameExcel);
        Sheet sheet = writeExcelBuilder.getWorkbook().getSheetAt(0);
        writeExcelBuilder.startSheet(sheet, dataConfig.getRowStart(), 0);
        writeExcelBuilder.setContentForDSDuAn(data);
        return writeExcelBuilder.build();
    }
}
