/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jun 2, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jun 2, 2020
 */

package vn.tcx.app.utils;

/**
 * Class CommonConstants.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jun 2, 2020
 */
public class CommonConstants {
    /**
     * EMPTY_STRING.
     */
    public static final String EMPTY_STRING = "";

    /**
     * DATE_FORMAT_SPLASH.
     */
    public static final String DATE_FORMAT_SPLASH = "yyyy/MM/dd";

    /**
     * Constant DD_MM_YYYY.
     */
    public static final String DD_MM_YYYY = "dd/MM/yyyy";

    /** The Constant NUM_0. */
    public static final int NUM_0 = 0;

    /** The Constant NUM_0DBL. */
    public static final double NUM_0DBL = 0.0;

    /** NUM_1. */
    public static final int NUM_1 = 1;

    /** NUM_1. */
    public static final int MINUS_1 = -1;

    /** NUM_2. */
    public static final int NUM_2 = 2;

    /** NUM_3. */
    public static final int NUM_3 = 3;

    /** NUM_4. */
    public static final int NUM_4 = 4;

    /** NUM_5. */
    public static final int NUM_5 = 5;

    /** NUM_6. */
    public static final int NUM_6 = 6;

    /** NUM_7. */
    public static final int NUM_7 = 7;

    /** NUM_8. */
    public static final int NUM_8 = 8;

    /** NUM_9. */
    public static final int NUM_9 = 9;

    /** The Constant NUM_10. */
    public static final int NUM_10 = 10;

    /**
     * CSV_EXTENSION The extension of file.
     */
    public static final String CSV_EXTENSION = ".csv";

    /**
     * Constant SLASH.
     */
    public static final String SLASH = "/";

    /**
     * Constant DOT.
     */
    public static final String DOT = ".";

    /**
     * COMMAS.
     */
    public static final String COMMAS = ",";

    /**
     * Constant SPACE.
     */
    public static final String SPACE = " ";

    /**
     * Constant PERCENT_STRING.
     */
    public static final String PERCENT_STRING = "%";

    /**
     * COLON.
     */
    public static final String COLON = ":";

    /**
     * PDF_EXTENSION The extension of file.
     */
    public static final String PDF_EXTENSION = ".pdf";

    /**
     * Constant PDF.
     */
    public static final String PDF = "pdf";

    /**
     * Constant DOCX.
     */
    public static final String DOCX = "docx";

    /**
     * Constant PNG.
     */
    public static final String PNG = "png";

    /**
     * Constant JPG.
     */
    public static final String JPG = "jpg";

    /**
     * Constant JPEG.
     */
    public static final String JPEG = "jpeg";

    /**
     * The extension of excel file.
     */
    public static final String XLSX = "xlsx";

    /**
     * Constant XLS.
     */
    public static final String XLS = "xls";

    /**
     * Instantiates a new common constants.
     *
     * @update PhucDD
     * @lastModifier Jul 29, 2020
     */
    private CommonConstants() {
        super();
    }
}
