/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Feb 4, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Feb 4, 2020
 */

package vn.tcx.app.utils;

import java.io.IOException;
import java.util.Iterator;

import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import vn.tcx.app.excel.config.DataConfig;

/**
 * The Class CommonUtils.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jun 2, 2020
 */
@Slf4j
public class CommonUtils {
    /**
     * Checks if is numeric.
     *
     * @param str the str
     * @return true, if is numeric
     */
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Checks if is empty.
     *
     * @param obj the obj
     * @return true, if is empty
     */
    public static boolean isEmpty(final Object obj) {
        return obj == null;
    }

    /**
     * Checks if is empty.
     *
     * @param obj the obj
     * @return true, if is empty
     */
    public static boolean isEmpty(final String obj) {
        return obj == null || CommonConstants.EMPTY_STRING.equals(obj.trim());
    }

    /**
     * Order by expression.
     *
     * @update PhucDD
     * @lastModifier Jun 2, 2020
     * @param sort the sort
     * @return the string
     */
    public static String orderByExpression(Sort sort) {
        StringBuilder sb = new StringBuilder();

        for (Iterator<Order> it = sort.iterator(); it.hasNext();) {
            Order order = it.next();
            sb.append(order.getProperty()).append(' ').append(order.getDirection());

            if (it.hasNext()) {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

    /**
     * Repeat numberic.
     *
     * @update PhucDD
     * @lastModifier Jun 2, 2020
     * @param character the character
     * @param numerical the numerical
     * @return string
     */
    public static String repeatNumberic(String character, int numerical) {
        if (character == null) {
            return null;
        }

        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numerical; i++) {
            sb.append(character);
        }

        return sb.toString();
    }
    
    /**
     * Lay data config.
     *
     * @update GreenGlobal
     * @lastModifier Jul 22, 2020
     * @param fileNameConfig the file name config
     * @return data config
     */
    public static DataConfig getDataConfig(String fileNameConfig) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(new ClassPathResource(fileNameConfig).getFile(), DataConfig.class);
        } catch (JsonGenerationException ex) {
            log.debug("Exception json generation {0}", ex.getMessage());
        } catch (JsonMappingException ex) {
            log.debug("Exception json mapping {0}", ex.getMessage());
        } catch (IOException ex) {
            log.debug("Exception IO exception {0}", ex.getMessage());
        }

        return null;
    }
}
