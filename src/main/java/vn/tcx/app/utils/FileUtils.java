/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jun 2, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jun 2, 2020
 */

package vn.tcx.app.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.net.HttpHeaders;

/**
 * Class FileUtils.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jun 2, 2020
 */
public class FileUtils {
    /**
     * Enum fileExtensionEnum.
     *
     * @author GreenGlobal
     * @since 1.0
     * @created Jun 2, 2020
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Extension {
        /**
         * doc.
         */
        DOC("doc"),

        /**
         * docx.
         */
        DOCX("docx"),

        /**
         * xls.
         */
        XLS("xls"),

        /**
         * xlsx.
         */
        XLSX("xlsx"),

        /**
         * ppt.
         */
        PPT("ppt"),

        /**
         * pptx.
         */
        PPTX("pptx"),

        /**
         * jpg.
         */
        JPG("jpg"),

        /**
         * jpge.
         */
        JPGE("jpge"),

        /**
         * png.
         */
        PNG("png"),

        /**
         * pdf.
         */
        PDF("pdf"),

        /**
         * zip.
         */
        ZIP("zip");

        /**
         * Instantiates a new file extension enum.
         *
         * @update PhucDD
         * @lastModifier Jun 2, 2020
         * @param ten the ten
         */
        private Extension(String ten) {
            this.ten = ten;
        }

        /**
         * Lay ten.
         *
         * @update GreenGlobal
         * @lastModifier Jun 2, 2020
         * @return ten
         */
        public String getTen() {
            return ten;
        }

        /**
         * ten.
         */
        private String ten;
    }

    /**
     * Download file http request with content type
     * <p>
     * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
     * </p>
     *
     * @update PhucDD
     * @lastModifier Jun 2, 2020
     * @param filename the filename
     * @param content  the content
     * @return ResponseEntity
     */
    public static ResponseEntity<byte[]> downloadFile(String filename, byte[] content) {
        switch (Extension.valueOf(FilenameUtils.getExtension(filename).toUpperCase())) {
          case DOC:
              return getResponseEntity("application/msword", filename, content);
          case DOCX:
              return getResponseEntity("application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                       filename, content);
          case XLS:
              return getResponseEntity("application/vnd.ms-excel", filename, content);
          case XLSX:
              return getResponseEntity("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                       filename, content);
          case PPT:
              return getResponseEntity("application/vnd.ms-powerpoint", filename, content);
          case PPTX:
              return getResponseEntity("application/vnd.openxmlformats-officedocument.presentationml.presentation",
                                       filename, content);
          case JPG:
          case JPGE:
              return getResponseEntity("image/jpeg", filename, content);
          case PNG:
              return getResponseEntity("image/png", filename, content);
          case PDF:
              return getResponseEntity("application/pdf", filename, content);
          case ZIP:
              return getResponseEntity("multipart/x-zip", filename, content);
          default:
              return getResponseEntity(MediaType.APPLICATION_OCTET_STREAM_VALUE, filename, content);
        }
    }

    /**
     * get response entity
     * 
     * @param mediaType
     * @param filename
     * @param content
     * @return ResponseEntity
     */
    private static ResponseEntity<byte[]> getResponseEntity(String mediaType, String filename, byte[] content) {
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaType))
                .header(HttpHeaders.CONTENT_DISPOSITION, filename).contentLength(content.length).body(content);
    }
}
