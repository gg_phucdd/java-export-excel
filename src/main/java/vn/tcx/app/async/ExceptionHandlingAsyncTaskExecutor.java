/*
 * Copyright 2020 (C) Green Global
 * 
 * Created on : Jul 16, 2020
 * Author     : Green Global
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * 1.0     AUTHOR :      Green Global
 * OLD     DATE   :      Jul 16, 2020
 */

package vn.tcx.app.async;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.task.AsyncTaskExecutor;

/**
 * Class ExceptionHandlingAsyncTaskExecutor.
 *
 * @author GreenGlobal
 * @since 1.0
 * @created Jul 16, 2020
 */
public class ExceptionHandlingAsyncTaskExecutor implements AsyncTaskExecutor, InitializingBean, DisposableBean {

    /**
     * log.
     */
    private final Logger log = LoggerFactory.getLogger(ExceptionHandlingAsyncTaskExecutor.class);

    /**
     * executor.
     */
    private final AsyncTaskExecutor executor;

    /**
     * Instantiates a new exception handling async task executor.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param executor the executor
     */
    public ExceptionHandlingAsyncTaskExecutor(AsyncTaskExecutor executor) {
        this.executor = executor;
    }

    /**
     * Execute.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param task the task
     */
    @Override
    public void execute(Runnable task) {
        executor.execute(createWrappedRunnable(task));
    }

    /**
     * Execute.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param task         the task
     * @param startTimeout the start timeout
     */
    @Override
    public void execute(Runnable task, long startTimeout) {
        executor.execute(createWrappedRunnable(task), startTimeout);
    }

    /**
     * Creates the callable.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param <T>  the generic type
     * @param task the task
     * @return the callable
     */
    private <T> Callable<T> createCallable(final Callable<T> task) {
        return () -> {
            try {
                return task.call();
            } catch (Exception e) {
                handle(e);
                throw e;
            }
        };
    }

    /**
     * Creates the wrapped runnable.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param task the task
     * @return the runnable
     */
    private Runnable createWrappedRunnable(final Runnable task) {
        return () -> {
            try {
                task.run();
            } catch (Exception e) {
                handle(e);
            }
        };
    }

    /**
     * Handle.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param e the e
     */
    protected void handle(Exception e) {
        log.error("Caught async exception", e);
    }

    /**
     * Submit.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param task the task
     * @return the future
     */
    @Override
    public Future<?> submit(Runnable task) {
        return executor.submit(createWrappedRunnable(task));
    }

    /**
     * Submit.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @param <T>  the generic type
     * @param task the task
     * @return the future
     */
    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return executor.submit(createCallable(task));
    }

    /**
     * Destroy.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @throws Exception exception
     */
    @Override
    public void destroy() throws Exception {
        if (executor instanceof DisposableBean) {
            DisposableBean bean = (DisposableBean) executor;
            bean.destroy();
        }
    }

    /**
     * After properties set.
     *
     * @update PhucDD
     * @lastModifier Jul 16, 2020
     * @throws Exception exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        if (executor instanceof InitializingBean) {
            InitializingBean bean = (InitializingBean) executor;
            bean.afterPropertiesSet();
        }
    }
}
